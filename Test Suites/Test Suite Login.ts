<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0400b012-e083-4b73-8c64-2c70a57fb2f5</testSuiteGuid>
   <testCaseLink>
      <guid>0938e118-ca27-4e8e-b616-d0e651f0ca58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login-testDriven</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7aa81b43-7c53-4c82-8259-d01929b082da</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>7aa81b43-7c53-4c82-8259-d01929b082da</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>3aa541be-7309-4bb8-8829-daee68b6b5c9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7aa81b43-7c53-4c82-8259-d01929b082da</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d5c7534b-364d-4108-86f8-eb41d795b26b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
